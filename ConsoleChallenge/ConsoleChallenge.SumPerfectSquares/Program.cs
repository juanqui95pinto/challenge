﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleChallenge.SumPerfectSquares
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int data = 19;
            var cuadrados = GeneraCuadradosPerfectos(data);


            foreach (var item in cuadrados)
            {
                Console.Write(item + ", ");
            }



            Console.WriteLine("NSquaresFor");

            //for (int i = 15; i < 20; i++)
            //{
            //    Console.WriteLine(NSquaresFor(i));
            //}

            Console.WriteLine(NSquaresFor(data));
            Console.ReadKey();
        }

        private static int NSquaresFor(int data)
        {
            var cuadrados = GeneraCuadradosPerfectos(data);
            var porResta = EncontrarMenorCantidadQueSumaCuadradosPorResta(data, cuadrados);

            return porResta;
        }

        private static int EncontrarMenorCantidadQueSumaCuadradosPorResta(int entrada, List<int> cuadrados)
        {
            int iteraciones = cuadrados.Count/2;
            List<int> resp = new List<int>();
            List<int> respuestas = new List<int>();
            List<int> noCuenta = new List<int>();
            bool cuenta = false;
            int data = entrada;
            int num = data;


            for (int i = 0; i <= iteraciones; i++)
            {
                cuenta = true; data = entrada; num = data; resp.Clear();
                while (num > 0)
                {
                    noCuenta.RemoveAll(s => s < data);
                    if (cuadrados.Contains(num) && !noCuenta.Contains(num))
                    {
                        resp.Add(num);
                        if (cuenta)
                        {
                            noCuenta.Add(num);
                            cuenta = false;
                        }
                        data = 0;
                        num = data;
                    }
                    else if (data > 0)
                    {
                        num--;
                        if (cuadrados.Contains(num) && !noCuenta.Contains(num))
                        {
                            resp.Add(num);
                            if (cuenta)
                            {
                                noCuenta.Add(num);
                                cuenta = false;
                            }
                            data = data - num;
                            if (data >= num)
                            {
                                noCuenta.RemoveAll(s => s < data);
                            }
                            num = data;
                        }
                    }
                }
                respuestas.Add(resp.Count());
            }

            Console.WriteLine("------------");
            foreach (var item in respuestas)
            {
                Console.WriteLine(item);
            }

            return respuestas.Min();
        }

        private static List<int> GeneraCuadradosPerfectos(int n)
        {
            List<int> cuadradosPerfectos = new List<int>();

            for (int i = 0; i < n; i++) //probar con foreach
            {
                cuadradosPerfectos.Add((i + 1) * (i + 1));
                //Console.WriteLine("generando Cuadr Perf: "+ (i + 1) * (i + 1));
                if ((i + 1) * (i + 1) >= n)
                {
                    break;
                }
            }

            return cuadradosPerfectos;
        }

    }
}