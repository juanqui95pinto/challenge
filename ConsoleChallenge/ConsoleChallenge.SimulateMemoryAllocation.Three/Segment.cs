﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulateMemoryAllocation.Three
{
    public class Segment
    {
        public string Name { get; set; }
        public int Size { get; set; }
        public bool Active { get; set; }

        public bool IsFree => Name == null;

        public void Deallocate()
        {
            Name = null;
        }
    }
}
