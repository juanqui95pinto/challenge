﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulateMemoryAllocation.Three
{
    public class MemoryView
    {
        public void CurrentStateOfMemory(List<Segment> objectSegments) //allocate and deallocate
        {
            Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine("\nMEMORY BUSY ▓ - MEMORY AVAILABLE ░");
            foreach (var item in objectSegments)
            {
                Console.WriteLine(item.Name  + " "+ item.Size);
            }

            foreach (var item in objectSegments)
            {
                for (int i = 1; i <= item.Size; i++)
                {
                    if (item.Name == null)
                    {
                        Console.Write("░");
                    }
                    else if (item.Size == i)
                    {
                        Console.Write("|");
                    }
                    else
                    {
                        Console.Write("▓");
                    }
                }
            }

            Console.ResetColor();
        }

        public void EmptySegment(int size)
        {
            Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine("\nWe have an empty memory segment of " + size + " MB - example");
            for (int i = 0; i < size; i++)
            {
                Console.Write("░");
            }

            Console.ResetColor();
        }
    }
}
