﻿using ConsoleChallenge.SimulateMemoryAllocation.Three.Algorithms;
using ConsoleChallenge.SimulateMemoryAllocation.Three.SimulationMemory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulateMemoryAllocation.Three
{
    public class ConsoleView
    {
        public IAlgorithmFit ChooseAlgorithm()
        {
            ConsoleKeyInfo KEY;
            Console.WriteLine("\n SELECT ONE OPTION:");
            Console.WriteLine("Fisrt Fit, press key 1");
            Console.WriteLine("Best Fit,  press key 2");
            Console.WriteLine("Worst Fit, press key 3");
            KEY = Console.ReadKey(true);
           
            switch (KEY.Key)
            {
                case ConsoleKey.D1:
                    return new FirstFit();
                    break;
                case ConsoleKey.D2:
                    return new BestFit();
                    break;
                case ConsoleKey.D3:
                    return new WorstFit();
                    break;
            }
            return new FirstFit();
        }

        internal string InputName()
        {
            Console.Write("\n Input NAME for Deallocate: ");
            return Console.ReadLine().ToUpper();
        }

        internal bool Continue()
        {
            ConsoleKeyInfo KEY;
            Console.Write("\n Do you want to Continue, press key Y");
            KEY = Console.ReadKey(true);
            if (KEY.Key == ConsoleKey.Y)
            {
                return true;
            }
            return false;
        }

        internal void VeryBigSegment(string name)
        {
            Console.WriteLine("\n dont find memory segment for " + name +"!!" );
        }

        internal int SizeMemory()
        {
            Console.Write("Input size of memory: ");
            return Convert.ToInt32(Console.ReadLine());
        }

        public Segment ReadObjectSegment() // for allocate memory
        {
            Segment objectSegment = new Segment();
            Console.WriteLine("\n Write data for object-segment");
            Console.Write("(one letter a-z) Input name of object: ");
            objectSegment.Name = Console.ReadLine().ToUpper();
            Console.Write("(unit in megabytes) Input size of object: ");
            
            objectSegment.Size = Convert.ToInt32(Console.ReadLine());
            objectSegment.Active = true;

            return objectSegment;
        }

        public bool SelectOperation() 
        {
            ConsoleKeyInfo KEY;
            Console.WriteLine("\n SELECT ONE OPTION:");
            Console.WriteLine("Allocate object to memory, press key 1");
            Console.WriteLine("Deallocate object from memory,  press key 2");
            KEY = Console.ReadKey(true);

            switch (KEY.Key)
            {
                case ConsoleKey.D1:
                    return true;
                    break;
                case ConsoleKey.D2:
                    return false;
                    break;
            }
            return true;
        }

        public string DeleteObjectSegment() // for deallocate memory
        {
            Console.WriteLine("Enter the initial of the ObjectSegment to delete:");
            return Console.ReadLine();
        }
    }
}
