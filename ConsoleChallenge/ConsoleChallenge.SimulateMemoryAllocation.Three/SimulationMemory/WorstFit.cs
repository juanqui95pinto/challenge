﻿using ConsoleChallenge.SimulateMemoryAllocation.Three.SimulationMemory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulateMemoryAllocation.Three.Algorithms
{
    public class WorstFit : IAlgorithmFit
    {
        public string Name
        {
            get => Name;
            set { Name = "WorstFit"; }
        }

        public Segment Find(int size, List<Segment> objectSegments)
        {
            int mayor = int.MinValue;
            Segment result = null;
            foreach (var objSeg in objectSegments)
            {
                if (size == objSeg.Size && objSeg.Name == null)
                {
                    return objSeg;
                }
                else if (size <= objSeg.Size && objSeg.Name == null)
                {
                    if (mayor < (objSeg.Size - size))
                    {
                        mayor = (objSeg.Size - size);
                        result = objSeg;
                    }
                }
            }

            return result;
        }
    }
}
