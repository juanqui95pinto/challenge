﻿using ConsoleChallenge.SimulateMemoryAllocation.Three.SimulationMemory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulateMemoryAllocation.Three.Algorithms
{
    public class FirstFit: IAlgorithmFit
    {
        public string Name 
        { 
            get => Name;
            set { Name = "FirstFit"; } 
        }

        public Segment Find(int size, List<Segment> objectSegments)
        {
            foreach (var objSeg in objectSegments)
            {
                if (size <= objSeg.Size && objSeg.Name == null)
                {
                    return objSeg;
                }
            }

            return null;
        }
    }
}
