﻿using ConsoleChallenge.SimulateMemoryAllocation.Three.Algorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulateMemoryAllocation.Three.SimulationMemory
{
    public class AppMemory: IMemoryFit  //esto sera como el main
    {
        public ConsoleView consoleView = new ConsoleView();
        public MemoryView memoryView = new MemoryView();
        public int memorySize;
        List<Segment> segments = new List<Segment>();
        IAlgorithmFit algorithm;

        public AppMemory(int memorySize, IAlgorithmFit algorithm)//
        {
            this.memorySize = memorySize;//megas like example
            this.algorithm = algorithm;
            var initialSegment = new Segment();
            initialSegment.Size = memorySize;
            segments.Add(initialSegment);
        }

        public void Allocate(int size, string name)//para elegir algoritmo patron Strategy
        {
            var block = algorithm.Find(size, segments);
            if (block == null)
            {
                consoleView.VeryBigSegment(name);
                return;//mesaje que rechaza por el tamanio
            }
            Segment[] bloquesDividos = Dividir(block, size);
            int idxAntigui = segments.IndexOf(block);
            segments.Insert(idxAntigui, bloquesDividos[0]);
            bloquesDividos[0].Name = name;
        }

        

        private Segment[] Dividir(Segment block, int size)
        {
            var resultado = new Segment[2];
            resultado[0] = new Segment();
            resultado[0].Size = size;
            resultado[1] = block;
            block.Size -= size;

            return resultado;
        }

        public void Deallocate(string name)
        {
            var segment = FindByName(name);
            segment.Deallocate();
            var next = GetNext(segment);
            var previous = GetPrevious(segment);
            MergeIfFree(segment, previous);
            MergeIfFree(segment, next);
        }

        private void MergeIfFree(Segment a, Segment b)
        {
            if (b == null) return;
            if (!b.IsFree) return;

            a.Size += b.Size;
            segments.Remove(b);
        }

        private Segment GetNext(Segment segment)
        {
            int idx = segments.IndexOf(segment);
            
            if (idx + 1 == segments.Count) return null;

            return segments[idx + 1];
        }

        private Segment GetPrevious(Segment segment)
        {
            int idx = segments.IndexOf(segment);

            if (idx == 0) return null;

            return segments[idx - 1];
        }

        private Segment FindByName(string name)
        {
            return segments.FirstOrDefault(s => s.Name == name);
        }

        public Segment GetObjectSegment()
        {
            return consoleView.ReadObjectSegment();
        }

        public bool IsAllocate(AppMemory appMemory)
        {
            if (appMemory.segments.Count == 1)
            {
                return true;
            }
            else if (consoleView.SelectOperation())
            {
                return true;
            }

            return false;
        }
        
        public void ShowMemory()
        {
            memoryView.CurrentStateOfMemory(segments);//mal nombre
        }
    }
}
