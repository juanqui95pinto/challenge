﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulateMemoryAllocation.Three.SimulationMemory
{
    public interface IAlgorithmFit
    {
        Segment Find(int size, List<Segment> os);
    }
}
