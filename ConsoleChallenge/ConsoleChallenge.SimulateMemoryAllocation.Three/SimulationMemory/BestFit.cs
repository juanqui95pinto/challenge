﻿using ConsoleChallenge.SimulateMemoryAllocation.Three.SimulationMemory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulateMemoryAllocation.Three.Algorithms
{
    public class BestFit : IAlgorithmFit
    {
        public string Name
        {
            get => Name;
            set { Name = "BestFit"; }
        }

        public Segment Find(int size, List<Segment> objectSegments)
        {
            int menor = int.MaxValue;
            Segment result = null;
            foreach (var objSeg in objectSegments)
            {
                if (size <= objSeg.Size && objSeg.Name == null)
                {
                    if (menor > (objSeg.Size - size))
                    {
                        menor = (objSeg.Size - size);
                        result = objSeg;
                    }
                }
            }

            return result;
        }
    }
}
