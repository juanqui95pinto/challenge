﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulateMemoryAllocation.Three.SimulationMemory
{
    public class StartFit
    {
        public List<Segment> DivideEmptyMemory(int memorySize, List<Segment> objectSegments)
        {
            string nombre = objectSegments[0].Name;
            int tamanoObj = objectSegments[0].Size;

            int newMemorySize = memorySize - tamanoObj;

            Segment memorySegment = new Segment();
            memorySegment.Name = "M"; // M de memory
            memorySegment.Size = newMemorySize;

            objectSegments.Add(memorySegment);

            return objectSegments;
        }
    }
}
