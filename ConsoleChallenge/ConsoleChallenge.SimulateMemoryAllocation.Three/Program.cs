﻿using ConsoleChallenge.SimulateMemoryAllocation.Three.Algorithms;
using ConsoleChallenge.SimulateMemoryAllocation.Three.SimulationMemory;
using System;

namespace ConsoleChallenge.SimulateMemoryAllocation.Three
{
    public class Program
    {
        static void Main(string[] args)
        {
            ConsoleView consoleView = new ConsoleView();
            AppMemory appMemory = new AppMemory(consoleView.SizeMemory(), consoleView.ChooseAlgorithm());
            appMemory.memoryView.EmptySegment(appMemory.memorySize);

            do {
                if (appMemory.IsAllocate(appMemory))
                {
                    Segment objSegment = consoleView.ReadObjectSegment();
                    appMemory.Allocate(objSegment.Size, objSegment.Name);
                    appMemory.ShowMemory();
                }
                else
                {
                    appMemory.Deallocate(consoleView.InputName());
                    appMemory.ShowMemory();
                }

            } while (consoleView.Continue());  

            //AppMemory appMemory = new AppMemory(100, new FirstFit());
            //AppMemory appMemory = new AppMemory(100, new BestFit());
            //AppMemory appMemory = new AppMemory(100, new WorstFit());

            //appMemory.Allocate(5, "A");
            //appMemory.Allocate(20, "B");
            //appMemory.Allocate(5, "C");
            //appMemory.Allocate(10, "D");
            //appMemory.Allocate(100, "E");

            //appMemory.ShowMemory();

            //appMemory.Deallocate("A");
            //appMemory.Deallocate("B");
            //appMemory.ShowMemory();
            //appMemory.Deallocate("C");
            //appMemory.ShowMemory();
            //appMemory.Deallocate("D");
            //appMemory.ShowMemory();
            //appMemory.Deallocate("E");
            //appMemory.ShowMemory();

            //appMemory.Allocate(3, "F");

            //appMemory.ShowMemory();

            // appMemory.Allocate(10, "D");
            //  appMemory.ShowMemory();
            //appMemory.ShowMemory();

        }
    }
}
