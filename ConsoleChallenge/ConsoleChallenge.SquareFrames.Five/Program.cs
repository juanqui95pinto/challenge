﻿using System;

namespace ConsoleChallenge.SquareFrames.Five
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            char[][] matriz = new char[20][50];
            int x, y;

            if (EsSoloEsquina(matriz, y, x))
            {

            }
        }

        private static bool EsSoloEsquina(char[][] matriz, int y, int x)
        {                         
            if (matriz[y][x] == '┐' && matriz[y][x- 1] == '*' && matriz[y + 1][x] == '*')
            {
                return true;
            }
            else if (matriz[y][x] == '└' && matriz[y][x + 1] == '*' && matriz[y - 1][x] == '*')
            {
                return true;
            }
            else if (matriz[y][x] == '┘' && matriz[y][x - 1] == '*' && matriz[y - 1][x] == '*')
            {
                return true;
            }

            return false;
        }
    }
}
