﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SquareFramess.four
{
    public class LoaderData : ILoaderData
    {
        //public System.IO.StreamReader fileTxt = new System.IO.StreamReader("..\\..\\..\\Resources\\simple example - 2 - Copy.txt");
        //public System.IO.StreamReader fileTxt = new System.IO.StreamReader("..\\..\\..\\Resources\\example.txt");
        //public System.IO.StreamReader fileTxt = new System.IO.StreamReader("..\\..\\..\\Resources\\example - Ivan.txt");
        //public System.IO.StreamReader fileTxt = new System.IO.StreamReader("..\\..\\..\\Resources\\example - Ivan2.txt");
        //public System.IO.StreamReader fileTxt = new System.IO.StreamReader("..\\..\\..\\Resources\\example - Ivan3.txt");
        //public System.IO.StreamReader fileTxt = new System.IO.StreamReader("..\\..\\..\\Resources\\example - Ivan4.txt");
        //public System.IO.StreamReader fileTxt = new System.IO.StreamReader("..\\..\\..\\Resources\\example - Ivan5.txt");
        public System.IO.StreamReader fileTxt = new System.IO.StreamReader("..\\..\\..\\Resources\\simple example.txt");
        //public System.IO.StreamReader fileTxt = new System.IO.StreamReader("..\\..\\..\\Resources\\simple example - 2.txt");
        //public System.IO.StreamReader fileTxt = new System.IO.StreamReader("..\\..\\..\\Resources\\squareTower.txt");
        string linea;

        public char[][] ReadFromFile()
        {
            char[][] canvasResult = new char[20][];
            int i = 0;

            while ((linea = fileTxt.ReadLine()) != null)
            {
                canvasResult[i] = linea.ToCharArray();
                i++;
            }

            return canvasResult;
        }
    }
}
