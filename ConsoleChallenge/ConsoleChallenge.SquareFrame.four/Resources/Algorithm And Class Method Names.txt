﻿

            Algoritmo de "Square Frames"
            ===========================
      Inicio          

    - Declaro una matriz 20*50 de tipo char
      leo el archivo.txt linea por linea mientras no llegue al fin del archivo
      y lo vamos guardando a la matriz a medida q lo leemos del archivo
      luego devolvemos la matriz llenado con los caracteres del archivo

    - En la matriz 20*50 recorrerlo linea por linea, caracter por caracter hasta encontrar esquina(s): 
      una vez encontrado una esquina por ej. ┌, se procede al Rastreo del cuadrado:
          recorrer horizontal desde la esquina superior izquierda '┌' mientras sea '─' linea, hasta '┐' esquina superior derecha, 
          recorrer vertical desde la esquina superior derecha '┐' mientras sea '│' linea, hasta '┘' la esquina inferior derecha, 
          recorrer horizontal inferior desde la esquina '┘' mientras sea '─' linea, hasta '└' la esquina inferior izquierda, 
          recorrer vertical desde la esquina '└' mientras sea '│' linea, hasta '┌' la esquina superior izquierda.
          completando asi el recorrido completo por las lineas q conforman el cuadrado.

    - Si completa el cuadrado:
        - lo guardamos los puntos X, Y iniciales, el tamanio del lado del cuadrado, con nivel = 1 en una lista de cuadrados
      Si no completa una vuelta completa al cuadrado
        - no lo tomamos en cuenta (no tomamos ninguna accion en este caso, solo sigue la siguiente celda de la matriz)
    - Dado q tenemos la lista de cuadrados de 1er-nivel, con los datos X,Y y lado //Cambiar por asteriscos(cuadrado, Nro de nivel)
        - cambiamos las lineas de estos cuadrados por astericos (como borrando los cuadrados de 1er-nivel)
          (realizando el recorrido '┌', '─'s, '┐', '│'s, '┘', '─'s, y '│'s, )

          =================================================================================================>>>>>>>>>>>>>>>>>>>>>>>   1 a 1.5hrs hasta aqui

┌─> **   (recorrido de cuadrado-incompleto con un lado)    //asteriscos como comodines    el anteriror tiene q ser si o si * y lineas logicas nada mas //no vale cortes no logicos
│   - Encontrar un lado de cuadrado de 2do-Nivel                                                                                   
│     En la matriz 20*50 recorrerlo linea por linea, caracter por caracter hasta encontrar esquina(s) '┌', 
│     //esto es para todas las esquinas ('┌', '┐', '└', ' ┘'); como ej tomamos '┌'.
│     Si '┌' recorre siguiendo la linea a la derecha hasta encontrar '┐'  //pasa *s tambien ┌─────***┐, pero no lineas de corte como ┌──│───┌─┐└─┘┐
│            y/o la linea hacia abajo hasta encontrar '└'                                                                             
│   - en el caso q una esquina logre llegar al otro extremo del cuadrado obtenemos un lado del cuadrado  
│     en el caso q no encuentre el correspondiente extremo intenta por el otro lado, si no lo encuentra // si encuentra un Anidate-Case No se toma actual esquina
│     en ninguno de los lados continua la siguiente casilla de la matriz
│                                                                                                                                      
│   - Dado que tenemos una de las esquinas ('┌', '┐', '└', ' ┘') y lado del cuadrado,                                                 
│     Chekeamos, verificamos (calculando el recorrido que deberia seguir este cuadrado) Y (realizando el recorrido '┌', '─'s, '┐', '│'s, '┘', '─'s, y '│'s, ) 
│     realizamos el recorrido aceptando asteriscos (*) para completar el recorrido (asteriscos y lineas)  //pasa *s tambien ┌─────***┐, pero no lineas de corte como ┌──│───┌─┐└─┘┐                             
│   - Lo guardamos los puntos X, Y (superiror izquierda), el tamanio del lado del cuadrado, con nivel = 2 en la lista de cuadrados    
│   - Dado q tenemos la lista de cuadrados de 2do-Nivel, con los datos X,Y y lado                                                     
│       - cambiamos las lineas de estos cuadrados por astericos (como borrando los cuadrados de 2do Nivel) //Cambiar por asteriscos(cuadrado, Nro de nivel)                           
│   - Volvemos al (** - recorrido de cuadrado-incompleto) para repetir asi con todas las esquinas ('┌', '┐', '└', ' ┘')  
└── - hasta completar el recorrido del toda la matriz de caracteres
// repetir el ciclo las veces q sea necesario, hasta completar todos los niveles
             ==============================================================================================>>>>>>>>>>>>>>>>>>>>> 2 a 2.5hrs esta parte

┌─> **   (recorrido de cuadrado-incompleto con una sola esquina)  //asteriscos como comodines
│   - Dado una esquina cualquiera ('┌', '┐', '└', ' ┘')
│     segun indica la esquina rastreamos los dos lados q tienen cada esquina (en horizotal y vertical)
│     Aceptando solamente linea logica (┌, ─, ┐, │, └, ┘) de cuadrado y asteriscos (*)
│   - cuando llega al extremo de cada lado, se calcula donde deberia corresponder o comprobar:///
│      - de estos 2 lados se toma el menor como el real y se toma ese para ambos lados
│     
│  ┌─> - una vez q se toma estos datos como verdaderos se realiza una vuelta de comprobacion del cuadrado 
│  │     (certificado) -->  (los otros 2 lados, y las 3 esquinas q faltan "de los extremos de la esquina y la diagonal-esquina")
│  │     y se guasda en la lista de cuadrados con el nivel correspondiente // //Cambiar por asteriscos(cuadrado, Nro de nivel)  
│  │    - cambiamos las lineas de estos cuadrados por astericos (como borrando los cuadrados de # Nivel) //Cambiar por asteriscos(cuadrado, Nro de nivel)  
│  │   -Caso q NO se compruebe:
│  └──────  se reduce en uno el lado y se vuelve a tratar //solo tomar en cuenta * y linea logica (┌, ─, ┐, │, └, ┘)
│           caso q no cumpla:
│           se deja sin tomar medidas (nada siempre)
└── - Luego volvemos a ** y repetimos para la siguiente casilla de la matriz
             ===========================================================================================>>>>>>>>>>>>>>>>>>>>> 2.5 a 3hrs esta parte
    - mostramos los resultados por consola:
      para el total de cuadrados mostramos el tamanio de la lista de cuadrados
      X, Y y lado de cada cuadrado segun nivel por orden desendente
      
      Fin
             ===========================================================================================>>>>>>>>>>>>>>>>>>>>> 0.5 a 1hr pruebas
                                                                                                              Total :         6 hrs a 7 hrs
                                                                                                             ===============================
      

NOTA: deberia dar una vuelta por nivel, comprobar y cambiar por *s serian metodos

//si en el camino ┌ pilla ┌┌┌ es q esta debajo de estos, solo puede pillar ─ o * 
//Litle Square-Case: cuando se forma un pequenio cuadrado con 2 cuadrados cruzados
//Anidate-Case: cuando en lado largo inicia y acaba un lado de un cuadrado  ┌──┌──┐──────┌──┐└────┘┌──┐─┐ //tendria q desechar el cuadrado grande y tomar < sin cortes




 sobre las clases  //Definir bien clase-responsabilidad-colaboradores // se deben mesionar, metodos publicos.
 ================

 Nombre Clase                 Responsabilidad                                    Colaboradores
 --------------------------------------------------------------------------------------------
 Program                    En el metodo Main()                                  clase Secuence
                            instancia a la clase Secuence y                      metodo BuildSquareSequence()
                            llama al metodo BuildSquareSequence()

 LoaderData                 Se encarga de leer el ejemplo.txt y llebarlo a la    varios ejemploNroENE.txt
                            matriz de caracteres llamado canvas[][]              donde ENE es un numero

 Square                     Contiene propiedades X, Y, Size, Level               ninguno, es como DTO 

 SquareSequence             Tiene lista de Square y Size (tamanio de la lista)   ninguno, es como DTO

 ISecuenceVisualizer        declara metodo Visualize(SquareSequence s)

 ConsoleSecuenceVisualizer  Se encarga de mostrar por consola los resultados     hereda metodo Visualize de ISecuenceVisualizer
                            
 ISequenceBuilder           declara metodo BuildSquareSequence()

 Sequence                   Donde esta la logica de la app, hereda de            ISequenceBuilder
                            ISequenceBuilder el metodo BuildSquareSequence()     
                            ahi estara la logica principal de la app.
                            Tambien tiene varios metodos q surgiran segun se
                            requiera en el camino

 CanvasSquare               Contiene el tablero de cuadrados o la matriz y
                            puede llenarse con FillCanvasWithSquare() que 
                            llama a LoaderData.LoadFromFile()
                            puede mostrarse con PrintCanvasSquare() que 
                            llama a ConsoleSecuenceVisualizer.Visualize()
 
 Tracker                    Considera si es posible q sea un cuadrado o
                            si aparentemente cumple con las condiciones
                            (de hecho q todos son cuadrados, pero en casos 
                            donde solo se ve una de las esquinas, 
                            no se tendria claro cual es el tamanio de sus 
                            lados).//caso happy path

 Checker                    Comprueba si realmente es un cuadrado de cierto
                            nivel o definitivamente no lo es
                            //caso realizar calculos para deducir/inferir
                            cuadrado


    Tips about this topic
    ---------------------

 5. Plan first, then code

 7. Code quality is very important. It should be self-documenting

10. First solve the problem, then code



Entre agregar detalles a lo q se tiene VS agregar pasos faltantes
agregar pasos faltantes tiene mas valor
