﻿using ConsoleChallenge.SquareFramess.four;
using System;

namespace ConsoleChallenge.SquareFrame.four
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleSecuenceVisualizer consoleSecuenceVisualizer = new ConsoleSecuenceVisualizer();

            var picProv = new PictureProvider(new LoaderData());
            SquarePicture canvasSquare = picProv.ProvidePicture();

            //canvasSquare.PrintCanvaSquares();
            SequenceBuilder sequence = new SequenceBuilder();
            var squareSequence = sequence.BuildSquareSequence(canvasSquare);
            consoleSecuenceVisualizer.Visualize(squareSequence);

            consoleSecuenceVisualizer.VisualizeMatrix(canvasSquare.canvas);

            //ConsoleSecuenceVisualizer consoleSecuenceVisualizer = new ConsoleSecuenceVisualizer();
            //var canvas = new char[20][];
            //for (int y = 0; y < canvas.Length; y++)
            //{
            //    canvas[y] = new char[50];
            //    for (int x = 0; x < canvas[y].Length; x++)
            //    {
            //        canvas[y][x] = '.';
            //    }
            //}
            //var pic = new SquarePicture(canvas);

            //var square = new Square();
            //square.Size = 5;
            //square.X = 6;
            //square.Y = 10;

            //pic.Draw(square);

            //var square2 = new Square();
            //square2.Size = 5;
            //square2.X = 8;
            //square2.Y = 10;

            //pic.Draw(square2);

            //consoleSecuenceVisualizer.VisualizeMatrix(canvas);
        }
    }
}
