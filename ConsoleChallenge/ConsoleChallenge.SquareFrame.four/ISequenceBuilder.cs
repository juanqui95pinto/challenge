﻿using ConsoleChallenge.SquareFramess.four;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SquareFrame.four
{
    interface ISequenceBuilder
    {
        SquareSequence BuildSquareSequence(SquarePicture canvasSquare);
    }
}
