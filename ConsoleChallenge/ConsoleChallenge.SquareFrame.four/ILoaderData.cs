﻿namespace ConsoleChallenge.SquareFramess.four
{
    public interface ILoaderData
    {
        char[][] ReadFromFile();
    }
}