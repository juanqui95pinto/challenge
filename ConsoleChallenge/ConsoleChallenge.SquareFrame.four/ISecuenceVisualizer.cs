﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SquareFramess.four
{
    interface ISecuenceVisualizer
    {
        void Visualize(SquareSequence s);
    }
}
