﻿namespace ConsoleChallenge.SquareFrame.four
{
    interface IPictureProvider
    {
        SquarePicture ProvidePicture();
    }
}