﻿using ConsoleChallenge.SquareFramess.four;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SquareFrame.four
{
    public class SquarePicture
    {
        public char[][] canvas;
        public SquarePicture(char[][] canvas)
        {
            this.canvas = canvas;
        }

        public void PrintCanvaSquares()
        {
            ConsoleSecuenceVisualizer consoleSecuenceVisualizer = new ConsoleSecuenceVisualizer();
            consoleSecuenceVisualizer.VisualizeMatrix(canvas);
        }

        internal void ChangeSquareToAsteriscs(SquareSequence squareSequence)
        {
            foreach (var square in squareSequence.Squares)
            {
                LineToAsterisk(square);
            }
        }

        private void LineToAsterisk(Square square)
        {
            int one = 1;

            for (int i = 0; i < square.Size; i++)
            {
                canvas[square.Y][square.X + i] = '*';
                canvas[square.Y + i][square.X] = '*';
                canvas[square.Y + square.Size - one][square.X + i] = '*';
                canvas[square.Y + i][square.X + square.Size - one] = '*';
            }
            PrintCanvaSquares();
        }

        internal void Draw(Square square)
        {
            int one = 1;
            int two = 2;
            canvas[square.Y][square.X] = '┌';
            canvas[square.Y][square.X + square.Size - one] = '┐';
            canvas[square.Y + square.Size - one][square.X] = '└';
            canvas[square.Y + square.Size - one][square.X + square.Size - one] = '┘';

            for (int i = one; i <= square.Size - two; i++)
            {
                canvas[square.Y][square.X+i] = '─';
                canvas[square.Y + i][square.X] = '│';
                canvas[square.Y + square.Size - one][square.X + i] = '─';
                canvas[square.Y + i][square.X + square.Size - one] = '│';
            }
            PrintCanvaSquares();
        }
    }
}
