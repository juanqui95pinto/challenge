﻿using ConsoleChallenge.SquareFramess.four;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SquareFrame.four
{
    class Tracker
    {
        private Square TryIdentifyOnFirstLevel(SquarePicture canvas, int y, int x)
        {
            Square square = new Square();
            int side = FindSideFromUpLeftToUpRight(canvas.canvas, y, x);
            int one = 1;
            int two = 2;
            bool lines = false;

            if (AreCorners(canvas.canvas, y, x, side))
            {
                if (side == two)
                    square = FillSquareData(y, x, side);

                for (int i = one; i <= side - two; i++)
                {
                    if (canvas.canvas[y][x + i] == '─' && canvas.canvas[y + i][x] == '│' &&
                    canvas.canvas[y + side - one][x + i] == '─' && canvas.canvas[y + i][x + side - one] == '│')
                    {
                        lines = true;
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            if (lines)
            {
                square = FillSquareData(y, x, side);
            }

            if (square.Level > 0)
            {
                return square;
            }

            return null;
        }

        private Square FillSquareData(int y, int x, int side)
        {
            Square square = new Square();
            int one = 1;
            square.X = x;
            square.Y = y;
            square.Size = side;
            square.Level = one;

            return square;
        }

        private bool AreCorners(char[][] canvas, int y, int x, int side)
        {
            int one = 0;
            if (side > 0)
            {
                one = 1;
            }
            if (canvas[y][x] == '┌' && canvas[y][x + side - one] == '┐' &&
            canvas[y + side - one][x] == '└' && canvas[y + side - one][x + side - one] == '┘')
            {
                return true;
            }

            return false;
        }

        private int FindSideFromUpLeftToUpRight(char[][] canvas, int y, int x)
        {
            int countX = x+1;
            int two = 2;
            if (canvas[y][x] == '┌' && canvas[y][countX] == '┐')  return two;

            if (canvas[y][x] == '┌')
            {
                while (canvas[y][countX] == '─')
                {
                    countX++;
                }

                if (canvas[y][countX] == '┐')
                {
                    return (countX - x) +1;
                }
            }

            return 0;
        }


        private Square TryIdentifyWithOneSide(SquarePicture canvasSquare, int y, int x)
        {
            Console.WriteLine(canvasSquare.canvas[y][x]);
            int side = FindAnySides(canvasSquare.canvas, y, x);

            if (InferSquare(canvasSquare.canvas, x, y, side))
            {
                Square sqr = new Square();
                sqr.Size = side;
                sqr.X = x;
                sqr.Y = y;

                return sqr;
            }

            return null;
        }

        private bool InferSquare(char[][] canvas, int x, int y, int side)
        {
            if (IsCornerFromSquare(canvas, x, y, side) && IsSideFromSquare(canvas, x, y, side))
            {
                return true;
            }
           
            return false;
        }

        private bool IsSideFromSquare(char[][] canvas, int x, int y, int side) 
        {
            if (CheckHorizontalsSidesFromSquare(canvas, y, x, side) && CheckVerticalSidesFromSquare(canvas, y,x, side))
            {
                return true;
            }

            return false;
        }

        private bool CheckVerticalSidesFromSquare(char[][] canvas, int y, int x, int side)
        {
            int one = 1;
            int two = 2;

            if (canvas[y][x] == '┌' && side > 2)
            {
                for (int i = one; i <= side - two; i++)
                {
                    if ((canvas[y + i][x] == '│' || canvas[y + i][x] == '*') &&
                        (canvas[y + i][x + side - 1] == '│' || canvas[y][x + side - 1] == '*'))
                    {
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else if (canvas[y][x] == '┐' && side > 2)
            {
                for (int i = one; i <= side - two; i++)
                {
                    if ((canvas[y + i][x] == '│' || canvas[y + i][x] == '*') &&
                        (canvas[y + i][x - side + 1] == '│' || canvas[y][x - side + 1] == '*'))
                    {
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else if (canvas[y][x] == '└' && side > 2)
            {
                for (int i = y -1; i > 0; i--)
                {
                    if ((canvas[y - i][x] == '│' || canvas[y - i][x] == '*') &&
                        (canvas[y - i][x + side - 1] == '│' || canvas[y - i][x + side - 1] == '*'))
                    {
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else if (canvas[y][x] == '┘' && side > 2)
            {
                for (int i = y - 1; i > 0; i--)
                {
                    if ((canvas[y - i][x] == '│' || canvas[y - i][x] == '*') &&
                        (canvas[y - i][x - side + 1] == '│' || canvas[y - i][x - side + 1] == '*'))
                    {
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private bool CheckHorizontalsSidesFromSquare(char[][] canvas, int y, int x, int side)
        {
            int one = 1;
            int two = 2;

            if (canvas[y][x] == '┌' && side > 2)
            {
                for (int i = one; i <= side - two; i++)
                {
                    if ((canvas[y][x + i] == '─' || canvas[y][x + i] == '*') &&
                        (canvas[y + side - 1][x + i] == '─' || canvas[y + side - 1][x + i] == '*'))
                    {
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else if (canvas[y][x] == '┐' && side > 2)
            {
                for (int i = one; i <= side - two; i++)
                {
                    if ((canvas[y][x - i] == '─' || canvas[y][x - i] == '*') &&
                        (canvas[y + side - 1][x - i] == '─' || canvas[y + side - 1][x -i] == '*'))
                    {
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else if (canvas[y][x] == '└' && side > 2)
            {
                for (int i = one; i <= side - two; i++)
                {
                    if ((canvas[y][x + i] == '─' || canvas[y][x + i] == '*') &&
                        (canvas[y - side + 1][x + i] == '─' || canvas[y - side + 1][x + i] == '*'))
                    {
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else if (canvas[y][x] == '┘' && side > 2)
            {
                for (int i = one - 1; i <= side - two; i++)
                {
                    if ((canvas[y][x - i] == '─' || canvas[y][x - i] == '*') &&
                        (canvas[y - side + 1][x - i] == '─' || canvas[y - side + 1][x - i] == '*'))
                    {
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool IsCornerFromSquare(char[][] canvas, int x, int y, int side)
        {
            int one = 0;
            if (side > 0)
            {
                one = 1;
            }

            if (canvas[y][x] == '┌')
            {
                if (canvas[y][x + side - one] == '┐' || canvas[y][x + side - one] == '*' &&
                    canvas[y + side - one][x + side - one] == '┘' || canvas[y + side - one][x + side - one] == '*' &&
                    canvas[y + side - one][x] == '└' || canvas[y + side - one][x] == '*')
                {
                    return true;
                }
            }
            else if (canvas[y][x] == '┐')
            {
                if (canvas[y + side - one][x] == '┘' || canvas[y + side - one][x] == '*' &&
                    canvas[y + side - one][x - side + one] == '└' || canvas[y + side - one][x - side + one] == '*' &&
                    canvas[y][x - side + one] == '┌' || canvas[y][x - side + one] == '*')
                {
                    return true;
                }
            }
            else if (canvas[y][x] == '┘')
            {
                if (canvas[y][x - side + one] == '└' || canvas[y][x - side + one] == '*' &&
                    canvas[y - side + one][x - side + one] == '┌' || canvas[y - side + one][x - side + one] == '*' &&
                    canvas[y - side + one][x] == '┐' || canvas[y - side + one][x] == '*')
                {
                    return true;
                }
            }
            else if (canvas[y][x] == '└')
            {
                if (canvas[y - side + one][x] == '┌' || canvas[y - side + one][x] == '*' &&
                    canvas[y - side + one][x + side - one] == '┐' || canvas[y - side + one][x + side - one] == '*' &&
                    canvas[y][x + side - one] == '┘' || canvas[y][x + side - one] == '*')
                {
                    return true;
                }
            }

            return false;
        }

        private int FindAnySides(char[][] canvas, int y, int x)
        {
            int result = 0;
            int sideX = 0;
            int sideY = 0;

            if (canvas[y][x] == '┌')
            {
                sideX = HorizontalMore(canvas, x, y);
                sideY = VerticalMore(canvas, x, y);

                return SelectedSide(sideX, sideY);
            }
           
            else if (canvas[y][x] == '└')
            {
                sideX = HorizontalMore(canvas, x, y);
                sideY = VerticalLess(canvas, x, y);

                return SelectedSide(sideX, sideY);
            }
            else if (canvas[y][x] == '┐')
            {
                sideX = HorizontalLess(canvas, x, y);
                sideY = VerticalMore(canvas, x, y);

                return SelectedSide(sideX, sideY);
            }
            else if (canvas[y][x] == '┘')
            {
                sideX = HorizontalLess(canvas, x, y);
                sideY = VerticalLess(canvas, x, y);

                return SelectedSide(sideX, sideY);
            }
           
            return result;
        }

        private int HorizontalLess(char[][] canvas, int x, int y)
        {
            int result = 0;
            int countX = x - 1;

            if (canvas[y][x] == '┌' && canvas[y][countX] == '┐' ||
                canvas[y][x] == '└' && canvas[y][countX] == '┘')
            {
                return 2;
            }

            while (canvas[y][countX] == '─')
            {
                countX--;
            }

            if (canvas[y][countX] == '┌' || canvas[y][countX] == '└')
            {
                return (x - countX) + 1;
            }

            return result;
        }

        private int VerticalLess(char[][] canvas, int x, int y)
        {
            int result = 0;
            int countY = y - 1;

            if (canvas[countY][x] == '┌' && canvas[countY][x] == '└' ||
                 canvas[countY][x] == '┐' && canvas[countY][x] == '┘')
            {
                return 2;
            }

            while (canvas[countY][x] == '│')
            {
                countY--;
            }

            if (canvas[countY][x] == '┌' || canvas[countY][x] == '┐')
            {
                return (y - countY) + 1;
            }

            return result;
        }

        private int SelectedSide(int sideX, int sideY)
        {
            if (sideX == 0 && sideY > 0)
            {
                return sideY;
            }
            if (sideX > 0 && sideY == 0)
            {
                return sideX;
            }
            
            if (sideX > 0 && sideY > 0)
            {
                if (sideX == sideY)
                {
                    return sideX;
                }
            }

            return 0;
        }

        private int VerticalMore(char[][] canvas, int x, int y)
        {
            int result = 0;
            int countY = y + 1;

            if (canvas[countY][x] == '┌' && canvas[countY][x] == '└' ||
                 canvas[countY][x] == '┐' && canvas[countY][x] == '┘')
            {
                return 2;
            }

            while (canvas[countY][x] == '│')
            {
                countY++;
            }

            if (canvas[countY][x] == '└' || canvas[countY][x] == '┘')
            {
                return (countY - y) + 1; 
            }

            return result;
        }

        private int HorizontalMore(char[][] canvas, int x, int y)
        {
            int result = 0;
            int countX = x +1;

            if (canvas[y][x] == '┌' && canvas[y][countX] == '┐' ||
                canvas[y][x] == '└' && canvas[y][countX] == '┘')
            {
                return 2;
            }

            while (canvas[y][countX] == '─')
            {
                countX++;
            }

            if (canvas[y][countX] == '┐' || canvas[y][countX] == '┘')
            {
                return (countX - x) + 1;
            }          

            return result;
        }

        private Square TryIdentifyWithOneCornerOnly(SquarePicture canvasSquare, int y, int x)
        {
            return null;
        }

        public Square TryIdentifySquare(SquarePicture canvas, int y, int x)
        {
            var square = TryIdentifyOnFirstLevel(canvas, y, x);

            if (square != null) return square;

            if (ThereAreAsterisk(canvas))
            {
                square = TryIdentifyWithOneSide(canvas, y, x);
                if (square != null) return square;
            }
           

            return TryIdentifyWithOneCornerOnly(canvas, y, x);
        }

        private bool ThereAreAsterisk(SquarePicture canvas)
        {
            for (int y = 0; y < canvas.canvas.Length; y++)
            {
                for (int x = 0; x < canvas.canvas[0].Length; x++)
                {
                    if (canvas.canvas[y][x] == '*')
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
