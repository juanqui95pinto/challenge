﻿using ConsoleChallenge.SquareFramess.four;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SquareFrame.four
{
    class PictureProvider : IPictureProvider
    {
        ILoaderData loader;
        public PictureProvider(ILoaderData loader)
        {
            this.loader = loader;
        }
        public SquarePicture ProvidePicture()
        {
            return new SquarePicture(loader.ReadFromFile());
        }
    }
}
