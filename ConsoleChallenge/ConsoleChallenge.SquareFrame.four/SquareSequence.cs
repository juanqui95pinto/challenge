﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SquareFramess.four
{
    public class SquareSequence
    {
        public List<Square> Squares; 
        public int Size;

        public SquareSequence()
        {
            this.Squares = new List<Square>();
        }
    }
}
