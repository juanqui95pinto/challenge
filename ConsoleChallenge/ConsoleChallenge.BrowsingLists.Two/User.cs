﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.BrowsingLists.Two
{
    public class User
    {
        public int Id { get; set; }
        public bool Active { get; set; }
        public string Name{ get; set; }
        public string Role { get; set; }
    }
}
