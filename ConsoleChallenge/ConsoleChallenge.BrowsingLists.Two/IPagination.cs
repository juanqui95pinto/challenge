﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.BrowsingLists.Two
{
    interface IPagination
    {
        public void First();
        public void Last();
        public void Next();
        public void Prev();
        public void Goto();
        public void Exit();
    }
}
