﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.BrowsingLists.Two
{
    public class ConsoleView
    {


        public void ShowMainMenu()
        {
            Console.WriteLine("  SELECT A KEY OPTION:  ");
            Console.WriteLine("\t First, pres Key: Home");
            Console.WriteLine("\t Last,  pres Key: End");
            Console.WriteLine("\t Next,  pres Key: RightArrow");
            Console.WriteLine("\t Prev,  pres Key: LeftArrow");
            Console.WriteLine("\t Goto,  pres Key: G or g");
            Console.WriteLine("\t Add User,  pres Key: U or u");
            Console.WriteLine("\t Exit,  pres Key: Escape");
        }

        public User ReadUserData()
        {
            User user = new User();
            Console.WriteLine("Input Name");
            user.Name = Console.ReadLine();
            user.Role = "User";

            return user;
        }
        
        public int ReadPageSize()
        {
            Console.Write("Input Size pagination: ");
            return Convert.ToInt32(Console.ReadLine());
        }

        public void ShowPage(List<User> users)
        {
            foreach (var user in users)
            {
                Console.WriteLine("-- Name: {0}, Role: {1}", user.Name, user.Role);
            }
        }
        
        public int ReadDestinationPage()
        {
            Console.Write("Input Goto pagination: ");
            return Convert.ToInt32(Console.ReadLine());
        }
    }
}
