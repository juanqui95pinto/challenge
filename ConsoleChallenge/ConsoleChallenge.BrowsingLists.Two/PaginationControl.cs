﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.BrowsingLists.Two
{
    public class PaginationControl : IPagination
    {
        Pagination pagination = new Pagination();
        ConsoleView consoleView = new ConsoleView();
        int oneValue = 1;

        public PaginationControl()
        {
            int paginationNumber = 2;
            int paginationSize = SetPaginationSize();
            
            LoadData();
            //SetPaginationSize();
            pagination.Size = paginationSize;
            pagination.PaginationNumber = paginationNumber;//esto cambia con los curores

            var queryResultPage = GetPageData(pagination);
        }
        public void MainMenu()//no deberia estar aqui, aqui solo control de pag, quizas en logic o bushiness
        {  
            bool continueApp = true;
            ConsoleKeyInfo KEY;
            do
            {
                consoleView.ShowMainMenu();
                KEY = Console.ReadKey(true);

                switch (KEY.Key) ///esto deberi ser un patron
                {
                    case ConsoleKey.Home:
                        First();
                        break;
                    case ConsoleKey.End:
                        Last();
                        break;
                    case ConsoleKey.RightArrow:
                        Next();
                        break;
                    case ConsoleKey.LeftArrow:
                        Prev();
                        break;
                    case ConsoleKey.G:
                        Goto();  
                        break;
                    case ConsoleKey.U:
                        CreateUser();  
                        break;
                    case ConsoleKey.Escape:
                        Exit();
                        break;
                }
            } while (continueApp);
        }
        public List<User> GetPageData(Pagination pagination)//
        {
            return pagination.users.Skip(pagination.Size * (pagination.PaginationNumber - 1)).Take(pagination.Size).ToList();
        }

        public void Exit()
        {
            Environment.Exit(1);
        }

        public void First()
        {
            pagination.PaginationNumber = 1;
            consoleView.ShowPage(GetPageData(pagination));
        }

        public void Goto()
        {
            pagination.PaginationNumber = consoleView.ReadDestinationPage();
            if (pagination.PaginationNumber > (pagination.users.Count() / pagination.Size) + oneValue)
            {
                Console.WriteLine("over index, select other number!!");
            }
            else
            {
                consoleView.ShowPage(GetPageData(pagination));
            }
        }

        public void Last()
        {
            pagination.PaginationNumber = (pagination.users.Count()/pagination.Size)+ oneValue;
            consoleView.ShowPage(GetPageData(pagination));
        }

        public void Next()
        {
            pagination.PaginationNumber = pagination.PaginationNumber + oneValue;
            consoleView.ShowPage(GetPageData(pagination));
        }

        public void Prev()
        {
            pagination.PaginationNumber = pagination.PaginationNumber - oneValue;
            consoleView.ShowPage(GetPageData(pagination));
        }

        public void CreateUser()
        {
            pagination.users.Add(consoleView.ReadUserData());
        }

        public int SetPaginationSize()
        {
            return consoleView.ReadPageSize();
        }

        private void LoadData()//deberia ser un obj
        {
            pagination.users.Add(new User { Name = "Maria Dolores Del Orto", Role = "user" });
            pagination.users.Add(new User { Name = "Omar Garita", Role = "user" });
            pagination.users.Add(new User { Name = "Elba Lazo", Role = "user" });
            pagination.users.Add(new User { Name = "Lola Mento", Role = "user" });
            pagination.users.Add(new User { Name = "Cindy Entes", Role = "user" });
            pagination.users.Add(new User { Name = "Elba Gina", Role = "user" });
            pagination.users.Add(new User { Name = "Rosa Melo", Role = "user" });
            pagination.users.Add(new User { Name = "Rosamel Delgado", Role = "user" });
            pagination.users.Add(new User { Name = "Paco Gerlo", Role = "user" });
            pagination.users.Add(new User { Name = "Armando Paredes", Role = "user" });
            pagination.users.Add(new User { Name = "Casimiro Lateta", Role = "user" });
            pagination.users.Add(new User { Name = "Lago Loza", Role = "user" });
            pagination.users.Add(new User { Name = "Elba Zurita", Role = "user" });
            pagination.users.Add(new User { Name = "Zoyla Becerra", Role = "user" });
            pagination.users.Add(new User { Name = "Alex Plosivo", Role = "user" });
            pagination.users.Add(new User { Name = "Ana Busado", Role = "user" });
            pagination.users.Add(new User { Name = "Ana Tomía", Role = "user" });
            pagination.users.Add(new User { Name = "Andrés Tresado", Role = "user" });
            pagination.users.Add(new User { Name = "Cindy Entes", Role = "user" });
            pagination.users.Add(new User { Name = "Cindy Versión", Role = "user" });
            pagination.users.Add(new User { Name = "Eddy Ficio", Role = "user" });
            pagination.users.Add(new User { Name = "Elba Tracio", Role = "user" });
            pagination.users.Add(new User { Name = "Elmer Kado", Role = "user" });
        }
    }
}
