using NUnit.Framework;
using ConsoleChallenge.SumsPerfectSquares;
using System;

namespace ConsoleChallenge.SumsPerfectSquares.Test
{
    public class AwesomeCalculator_Add_Should
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            Assert.Pass();
        }

        [Test]
        public void Return_2_Given_Value_Of_1_And_1()
        {
            ICalculator calculator = new AwesomeCalculator();
            var result = calculator.Add(1,1);
            Assert.Equals(2, result);
        }
    }
}