﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulatesMemoryAllocation.Three
{
    public class ObjectSegment
    {
        public string Initial { get; set; }
        public int Size { get; set; }
        public bool Active { get; set; }
    }
}
