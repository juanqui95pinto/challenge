﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulatesMemoryAllocation.Three
{
    public class FirstFit : IAlgorithm
    {
        public void Algorithm()//100Mb como base fijo, recibe uno por uno los procesos
        {

            //int[] processSize = new int[5]; //left rejectedProcess
            ObjectSegment objectSize = new ObjectSegment();
            objectSize.Initial = "A";
            objectSize.Size = 5;

            int[] oneHundredOfMemory = new int[100];//100MB //left memory
            int[] processAllocate = { 0, 0, 0, 0, 0 };
            //int[] processSizeCopy = { 100, 100, 400, 300, 100 };
            //int[] memorySizeCopy = { 500, 100, 200, 300, 100 };
            int empty = 0;

            for (int p = 0; p < objectSize.Size; p++)
            {
                for (int m = 0; m < oneHundredOfMemory.Length; m++)//al inicio si,
                {                   //luego sera la lista de obj.Active=False son los espacios
                                    //libres y no sabemos cuantos podrian ser, pero si no pasan
                                    //de cien (100)
                    if (oneHundredOfMemory[m] >= objectSize.Size)
                    {
                        processAllocate[m] += objectSize.Size;

                        oneHundredOfMemory[m] -= objectSize.Size;
                        objectSize.Size = empty;
                        if (oneHundredOfMemory[m] == 0)
                        {
                            break;
                        }
                    }
                }
            }

            //Console.WriteLine("Process\tRejctP\tMemory\tMemLeft\tprocAlloc");
            //for (int i = 0; i < processSize.Length; i++)
            //{
            //    Console.WriteLine(" {0},\t {1}, \t{2}, \t{3}, \t{4}", processSizeCopy[i], processSize[i], memorySizeCopy[i], memorySize[i], processAllocate[i]);
            //}
            
            #region old idea
            /*
            int[] processSize = { 100, 100, 400, 300, 100 };//left rejectedProcess
            int[] memorySize = { 500, 100, 200, 300, 0 }; //left memory
            int[] processAllocate = { 0, 0, 0, 0, 0 };
            int[] processSizeCopy = { 100, 100, 400, 300, 100 };
            int[] memorySizeCopy = { 500, 100, 200, 300, 100 };
            int empty = 0;

            for (int p = 0; p<processSize.Length; p++)
            {
                for (int m = 0; m<memorySize.Length; m++)
                {
                    if (memorySize[m] >= processSize[p])
                    {
                        processAllocate[m] += processSize[p];

                        memorySize[m] -= processSize[p];
                        processSize[p] = empty;
                        if (memorySize[m] == 0)
                        {
                            break;  
                        }
                    }
                }
            }

            Console.WriteLine("Process\tRejctP\tMemory\tMemLeft\tprocAlloc");
            for (int i = 0; i < processSize.Length; i++)
            {               
                Console.WriteLine(" {0},\t {1}, \t{2}, \t{3}, \t{4}", processSizeCopy[i], processSize[i], memorySizeCopy[i], memorySize[i], processAllocate[i] );
            }
            */
            #endregion
        }
    }
}
