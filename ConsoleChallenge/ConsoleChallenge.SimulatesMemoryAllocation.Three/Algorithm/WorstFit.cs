﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulatesMemoryAllocation.Three
{
    public class WorstFit:IAlgorithm
    {
        public void Algorithm()
        {
            int[] processSize = { 100, 100, 400, 300, 100 };//left rejectedProcess
            int[] memorySize = { 500, 100, 200, 300, 0 }; //left memory
            int[] processAllocate = { 0, 0, 0, 0, 0 };
            int[] processSizeCopy = { 100, 100, 400, 300, 100 };
            int[] memorySizeCopy = { 500, 100, 200, 300, 0 };
            int empty = 0;

            for (int p = 0; p < processSize.Length; p++)
            {
                for (int m = 0; m < memorySize.Length; m++)
                {
                    if (IsWorstFit(processSize[p], m, memorySize))
                    {
                        processAllocate[m] += processSize[p];

                        memorySize[m] -= processSize[p];
                        processSize[p] = empty;
                        if (memorySize[m] == 0)
                        {
                            break;
                        }
                    }
                }
            }

            Console.WriteLine("Process\tRejctP\tMemory\tMemLeft\tprocAlloc");
            for (int i = 0; i < processSize.Length; i++)
            {
                Console.WriteLine(" {0},\t {1}, \t{2}, \t{3}, \t{4}", processSizeCopy[i], processSize[i], memorySizeCopy[i], memorySize[i], processAllocate[i]);
            }
            #region old idea
            /*
            int[] processSize = { 100, 100, 400, 300, 100 };//left rejectedProcess
            int[] memorySize = { 500, 100, 200, 300, 0 }; //left memory
            int[] processAllocate = { 0, 0, 0, 0, 0 };
            int[] processSizeCopy = { 100, 100, 400, 300, 100 };
            int[] memorySizeCopy = { 500, 100, 200, 300, 0 };
            int empty = 0;

            for (int p = 0; p < processSize.Length; p++)
            {
                for (int m = 0; m < memorySize.Length; m++)
                {
                    if (IsWorstFit(processSize[p], m, memorySize))
                    {
                        processAllocate[m] += processSize[p];

                        memorySize[m] -= processSize[p];
                        processSize[p] = empty;
                        if (memorySize[m] == 0)
                        {
                            break;
                        }
                    }
                }
            }

            Console.WriteLine("Process\tRejctP\tMemory\tMemLeft\tprocAlloc");
            for (int i = 0; i < processSize.Length; i++)
            {
                Console.WriteLine(" {0},\t {1}, \t{2}, \t{3}, \t{4}", processSizeCopy[i], processSize[i], memorySizeCopy[i], memorySize[i], processAllocate[i]);
            }
            */
            #endregion
        }

        private bool IsWorstFit(int process, int memoryIndex, int[] memorySize)
        {
            bool result = false;
            int millon = -1000000;
            int[] aux = new int[memorySize.Length];

            for (int i = 0; i < memorySize.Length; i++)
            {
                if (memorySize[i] - process >= 0)
                {
                    aux[i] = memorySize[i] - process;
                }
                else if (memorySize[i] - process < 0)
                {
                    aux[i] = millon;
                }
            }
            int indexAux = aux.Max();
            for (int i = 0; i < memorySize.Length; i++)
            {
                if (aux.Max() == memorySize[i] - process)
                {
                    if (i == memoryIndex)
                    {
                        return true;
                    }
                }
            }

            return result;
        }

        public void InputData()
        {
            throw new NotImplementedException();
        }

        public void PrintResult()
        {
            throw new NotImplementedException();
        }
    }
}
