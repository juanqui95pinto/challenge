﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulatesMemoryAllocation.Three
{
    public class MemorySimulationDTO
    {
        public string AlgorithmName { get; set; }
        public ObjectSegment objSegment { get; set; }
    }
}
