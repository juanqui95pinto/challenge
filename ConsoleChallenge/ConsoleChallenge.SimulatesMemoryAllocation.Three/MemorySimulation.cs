﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulatesMemoryAllocation.Three
{
    public class MemorySimulation:IDynamicMemory
    {
        MemorySimulationDTO memorySimulationDTO = new MemorySimulationDTO();
        ObjectSegment objectSegment = new ObjectSegment();
        ConsoleView consoleView = new ConsoleView();

        public void Allocate()//llamar al algoritmo correspondiente y cargarlo
        {
            throw new NotImplementedException();
        }

        public void Deallocate()//quitar de la memoria el obj seleccionado
        {
            throw new NotImplementedException();
        }

        public void LoadData()
        {
            memorySimulationDTO.AlgorithmName = consoleView.ChooseAlgorithm();
            memorySimulationDTO.objSegment = consoleView.CreateObjectSegment();
        }

        public void start()
        {
            consoleView.EmptyMemorySegment();
            LoadData();
            Allocate();
            Deallocate();
            consoleView.EmptyMemorySegment();
        }
    }
}
