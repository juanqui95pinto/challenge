﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulatesMemoryAllocation.Three
{
    public interface IDynamicMemory
    {
        void Allocate();
        void Deallocate();
    }
}
