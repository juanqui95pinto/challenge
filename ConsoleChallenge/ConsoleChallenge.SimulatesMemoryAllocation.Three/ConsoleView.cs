﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SimulatesMemoryAllocation.Three
{
    public class ConsoleView
    {
        public string ChooseAlgorithm()
        {
            ConsoleKeyInfo KEY;
            Console.WriteLine("\r\nFisrt Fit, press key 1");
            Console.WriteLine("Best Fit,  press key 2");
            Console.WriteLine("Worst Fit, press key 3");
            Console.WriteLine("By default: key bar");
            KEY = Console.ReadKey(true);

            switch (KEY.Key)
            {
                case ConsoleKey.D1:
                    return "Fisrt Fit";
                    break;
                case ConsoleKey.D2:
                    return "Best Fit";
                    break;
                case ConsoleKey.D3:
                    return "Worst Fit";
                    break;
                case ConsoleKey.Spacebar:
                    return "Fisrt Fit";
                    break;
            }
            return "Fisrt Fit";
        }

        public ObjectSegment CreateObjectSegment() // for allocate memory
        {
            ObjectSegment objectSegment = new ObjectSegment();
            Console.Write("(una letra a-z) Input name of object: ");
            objectSegment.Initial = Console.ReadLine();
            Console.WriteLine();
            Console.Write("(unit in megabytes) Input size of object: ");
            objectSegment.Size = Convert.ToInt32(Console.ReadLine());
            objectSegment.Active = true;

            return objectSegment;
        }

        public string DeleteObjectSegment() // for deallocate memory
        {
            Console.WriteLine("Enter the initial of the ObjectSegment to delete:");
            return Console.ReadLine();
        }

        public void EmptyMemorySegment()
        {
            Console.ForegroundColor = ConsoleColor.Green;

            int mega = 100;
            Console.WriteLine("100MB - Empty memory segment");
            for (int i = 0; i < mega; i++)
            {
                if (i == 5)
                {
                    Console.Write("|");
                }
                Console.Write("░");
            }

            Console.ResetColor();
        }
    }
}
