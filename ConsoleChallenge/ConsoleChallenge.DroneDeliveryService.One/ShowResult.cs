﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.DroneDeliveryService.One
{
    public class ShowResult
    {
        public void ShowTrips(List<TripForDronDTO> trips) 
        {
            foreach (var trip in trips)
            {
                Console.WriteLine();
                Console.WriteLine("Dron: {0}", trip.Drone.Name);
                Console.WriteLine("Trip #{0}", trip.Number);//corregir a vuelos por Drone
                foreach (var location in trip.Locations)
                {
                    Console.Write("{0}-{1};  ", location.Name, location.PackegeWeight);
                }
                Console.WriteLine();
                Console.WriteLine();
            }
        }
    }
}
