﻿using System;

namespace ConsoleChallenge.DroneDeliveryService.One
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Drone Delivery Service!");

            DeliveryPlanner deliveryPlanner = new DeliveryPlanner();
            deliveryPlanner.AssignLocationsToDrones();
        }
    }
}
