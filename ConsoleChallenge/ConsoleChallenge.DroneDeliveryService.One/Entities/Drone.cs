﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.DroneDeliveryService.One
{
    public class Drone
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public int MaximumWeight { get; set; }
        public int Remaining { get; set; }
    }
}
