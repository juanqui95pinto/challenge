﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.DroneDeliveryService.One
{
    public class Location
    {
        public int Number { get; set; }
        public bool Active { get; set; }
        public string Name { get; set; }
        public int PackegeWeight { get; set; }
        public int Remaining { get; set; }
    }
}
