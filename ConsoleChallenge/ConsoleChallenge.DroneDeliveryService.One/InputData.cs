﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.DroneDeliveryService.One
{
    public class InputData
    {
        public System.IO.StreamReader archivo = new System.IO.StreamReader("..\\..\\..\\Resources\\DronesLocations.txt");
        string separador = ",";
        string linea;

        public List<Drone> ReadFileDrone()
        {
            List<Drone> drones = new List<Drone>();
            int numberDron = 1;
            linea = archivo.ReadLine();
            string[] fila = linea.Split(separador);

            for (int i = 0; i < fila.Length - 1; i++)
            {
                Drone drone = new Drone();
                drone.Number = numberDron;
                drone.Name = fila[i];
                i++;
                drone.MaximumWeight = Convert.ToInt32(fila[i]);
                drone.Remaining = drone.MaximumWeight;
                drones.Add(drone);
                numberDron++;
            }

            return drones;
        }

        public List<Location> ReadFileLocation()
        {
            List<Location> locations = new List<Location>();
            int numberLocation = 1;
          //  archivo.ReadLine(); 
            while ((linea = archivo.ReadLine()) != null)
            {
                string[] fila = linea.Split(separador);
                Location location = new Location();
                location.Number = numberLocation;
                location.Active = true;
                location.Name = fila[0];
                location.PackegeWeight = Convert.ToInt32(fila[1]);
                location.Remaining = Convert.ToInt32(fila[1]);
                locations.Add(location);
                numberLocation++;
            }

            return locations;
        }
    }
}
