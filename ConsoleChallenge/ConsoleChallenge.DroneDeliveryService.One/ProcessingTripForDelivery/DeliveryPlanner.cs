﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.DroneDeliveryService.One
{
    public class DeliveryPlanner
    {
        List<Drone> drones = new List<Drone>();
        List<Location> locations = new List<Location>();
        List<TripForDronDTO> trips = new List<TripForDronDTO>();

        public DeliveryPlanner()
        {
            InputData inputData = new InputData();

            this.drones = inputData.ReadFileDrone();
            this.locations = inputData.ReadFileLocation();
            this.trips = new List<TripForDronDTO>();
        }

        public void AssignLocationsToDrones()
        {            
            
            List<Drone> sortDrones = drones.OrderByDescending(d => d.MaximumWeight).ToList();
            List<Location> sortLocations = locations.OrderByDescending(l => l.PackegeWeight).ToList();
            ShowResult showResult = new ShowResult();


            foreach (var drone in sortDrones)
            {
                //Console.WriteLine("Drone: {0}, {1}", drone.Name, drone.MaximumWeight);
                TripForDronDTO trip = new TripForDronDTO();
                trip.Drone = drone;
                List<Location> locs = new List<Location>();
                foreach (var location in sortLocations)
                {
                    //Console.WriteLine("Location: {0}, {1}", location.Name, location.PackegeWeight);
                    while (SumPackegeWeight(locations) >= drone.MaximumWeight && location.Active)
                    {
                        Console.WriteLine(SumPackegeWeight(locations) + "======="+ drone.Name +" "+ location.Name  );
                        //cdo location.Remaining es mas grande (o igual q cap.drone) //osea varios Trips de drone por un solo Locator grande
                        while (location.Remaining >= drone.MaximumWeight && location.Active)  
                        {
                            location.Remaining = location.Remaining - drone.MaximumWeight;

                            if (location.Remaining == 0)
                            {
                                location.Active = false;
                            }
                            //cargar el trip
                        }
                        while (drone.Remaining >= location.PackegeWeight && location.Active)//otro caso, cuando location.Remaining es menor q cap.drone //osea en UN DRONE GRaNDE entran varios locators
                        {
                            drone.Remaining = drone.Remaining - location.PackegeWeight;
                            location.Active = false;

                            //cargar el lista de locs
                            locs.Add(location);
                        }
                        if (drone.Remaining == 0)
                        {
                            trip.Locations = locs;
                            trips.Add(trip);

                            //showResult.ShowTrips(trips);

                            locs.Clear();
                            drone.Remaining = drone.MaximumWeight;
                        }
                        //Console.WriteLine(result + "=======" + cont++);
                    }
                }
            }


            showResult.ShowTrips(trips);


            //foreach (var item in sortDrones) Console.WriteLine(item.Number + " " + item.Name + " " + item.MaximumWeight);
            //Console.WriteLine("--------------------------");
            //foreach (var item in sortLocations) Console.WriteLine(item.Number + " " + item.Name + " " + item.PackegeWeight + " " + item.Active);

        }

        private int SumPackegeWeight(List<Location> locations)
        {
            int result = 0; 

            foreach (var location in locations)
            {
                if (location.Active)
                {
                    result += location.Remaining;
                }
            }
            //Console.WriteLine(result+"=======");
            return result;
        }
    }
}
