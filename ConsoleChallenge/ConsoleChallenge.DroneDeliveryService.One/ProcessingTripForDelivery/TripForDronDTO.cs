﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.DroneDeliveryService.One
{
    public class TripForDronDTO
    {
        public int Number { get; set; }
        public Drone Drone { get; set; }
        public List<Location> Locations = new List<Location>(); 
    }
}
