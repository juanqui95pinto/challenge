﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SquareFramess.Four
{
    interface ISecuenceVisualizer
    {
        void Visualize(SquareSequence s);
    }
}
