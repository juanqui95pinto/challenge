﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SquareFramess.Four
{
    class ConsoleSecuenceVisualizer:ISecuenceVisualizer
    {
        public void Visualize(SquareSequence squares)
        {
            Console.WriteLine("\n\t\t" + squares.Size);
            Console.WriteLine("\n\tX \tY \tside");
            foreach (var item in squares.Squares)
            {
                Console.WriteLine("\t{0}, \t{1}, \t {2}", item.X, item.Y, item.Size);
            }
        }
    }
}
