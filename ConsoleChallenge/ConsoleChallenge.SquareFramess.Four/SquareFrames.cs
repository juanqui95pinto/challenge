﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallenge.SquareFramess.Four
{
    class SquareFrames
    {
        private int squareSize;
        private char[][] canvas;// = new char[squareSize][];
        SquareSequence squareSequence = new SquareSequence();
        ReadFileData readData = new ReadFileData();
        ConsoleSecuenceVisualizer consoleSecuenceVisualizer = new ConsoleSecuenceVisualizer();

        public SquareFrames()
        {
            this.squareSize = 20;
            this.canvas = new char[squareSize][];
            canvas = readData.SquareCharacters();
        }

        internal void App()
        {
            FindUpLeftCorners();
            consoleSecuenceVisualizer.Visualize(squareSequence);
            FindAnyCorners();
            

        }

        private void FindUpLeftCorners()
        {
            for (int y = 0; y < squareSize; y++)
            {
                for (int x = 0; x < canvas[y].Length; x++)
                {
                    if (canvas[y][x] == '┌')
                    {
                        Square square = FindLevelOneSquares(x, y);

                        if (square != null)
                        {
                            squareSequence.Squares.Add(square);
                            ReemplaceFromPoints(square);
                            squareSequence.Size = squareSequence.Squares.Count();
                        }
                    }
                }
            }
        }

        private void FindAnyCorners()
        {
            for (int y = 0; y < squareSize; y++)
            {
                for (int x = 0; x < canvas[y].Length; x++)
                {
                    if (IsCorner(canvas[y][x]))
                    {
                        Square square = FindNextLevelSquares(y, x);

                        if (square != null)
                        {
                            squareSequence.Squares.Add(square);
                            ReemplaceFromPoints(square);
                            squareSequence.Size = squareSequence.Squares.Count();
                        }
                    }
                }
            }
        }

        private bool IsCorner(char c)
        {
            return c == '┌' || c == '┐' || c == '└' || c == '┘';
        }

        private Square FindNextLevelSquares(int y, int x)
        {
            int lado = FindSizeSide(y, x);
            Square result = new Square();
            //if (canvas[y][x] == '┌' && GoLeft() && GoDown())
            //{

            //}

            return result;
        }

        private int FindSizeSide(int y, int x)
        {
            int result = 0;



            return result;
        }

        //private Square FindLevelOneSquares2(int x, int y)
        //{
        //    //dado esquina del cuadrado
        //    //encontrar el lado del cuadrado
        //    //en base al lado, verificar lados completos
        //    //guardarlos en 1er level
        //    int side = CalculateSizeOfSide()
        //}

        private Square FindLevelOneSquares(int beginX, int beginY)
        {
            int newX = beginX;
            int newY = beginY;
            newX++; newY++;
            int two = 0;
            if((int)canvas[beginY][beginX] == (int)'┌' && (int)canvas[beginY][newX] == (int)'┐' 
                && (int)canvas[newY][beginX] == (int)'└' && (int)canvas[newY][newX] == (int)'┘'
                && newX < canvas[0].Length && newY < squareSize)
            {
                return LoadResult(beginX, beginY, two);
            }
            

            int side = 0;
            bool flag = true;
            beginX++;

            while (flag && beginX < canvas[0].Length)
            {
                if ((int)canvas[beginY][beginX] == (int)'─')
                {
                    side++; beginX++;
                }
                else
                {
                    flag = false;
                }
            }

            if ((int)canvas[beginY][beginX] != (int)'┐')
            {
                return null;
            }
            //Console.WriteLine("--- x: {0}, y: {1}, cont: {2}", beginX, beginY, side + " " + canvas[beginY][beginX]);

            if (CheckLineToDown(beginY, beginX, side) > 0)
            {
                beginY = CheckLineToDown(beginY, beginX, side);
            }
            else
            {
                return null;
            }

            if (CheckLineToDownLeft(beginY, beginX, side) > 0)
            {
                beginX = CheckLineToDownLeft(beginY, beginX, side);
            }
            else
            {
                return null;
            }

            if (CheckLineToUp(beginY, beginX, side) > 0)
            {
                beginY = CheckLineToUp(beginY, beginX, side);
            }
            else
            {
                return null;
            }

            return LoadResult(beginX, beginY, side);
        }

        private Square LoadResult(int beginX, int beginY, int side)
        {
            Square result = new Square();
            int cornersBySide = 2;
            int one = 1;
            result.Level = one;
            result.X = beginX;
            result.Y = beginY;
            result.Size = side + cornersBySide;

            return result;
        }

        private int CheckLineToUp(int beginY, int beginX, int side)
        {
            beginY--;
            int LimY = beginY - side; 

            for (; beginY > LimY; beginY--)
            {
                if ((int)canvas[beginY][beginX] != (int)'│')
                {
                    return 0;
                }
            }

            if ((int)canvas[beginY][beginX] != (int)'┌')
            {
                return 0;
            }
            
            //Console.WriteLine("││││ x: {0}, y: {1}, cont: {2}", beginX, beginY, side + " " + canvas[beginY][beginX]);

            return beginY;
        }

        private int CheckLineToDownLeft(int beginY, int beginX, int side)
        {
            beginX--;
            int LimX = beginX - side;

            for (; beginX > LimX; beginX--)
            {
                if ((int)canvas[beginY][beginX] != (int)'─')
                {
                    return 0;
                }
            }

            if ((int)canvas[beginY][beginX] != (int)'└')
            {
                return 0;
            }

            //Console.WriteLine("--- x: {0}, y: {1}, cont: {2}", beginX, beginY, side + " " + canvas[beginY][beginX]);

            return beginX;
        }

        private int CheckLineToDown(int beginY, int beginX, int side)
        {
            beginY++;
            int limY = beginY + side;

            for (; beginY < limY; beginY++)
            {
                if ((int)canvas[beginY][beginX] != (int)'│')
                {
                    return 0; 
                }
            }

            if ((int)canvas[beginY][beginX] != (int)'┘')
            {
                return 0;
            }
            
            //Console.WriteLine("││││ x: {0}, y: {1}, cont: {2}", beginX, beginY, side + " " + canvas[beginY][beginX]);

            return beginY;
        }

        private void ReemplaceFromPoints(Square square)
        {
            //change for point X
            HorizontalsForPoints(square);
            //canvas[square.Y][square.X] = '*';
            //change for point Y

        }

        private void HorizontalsForPoints(Square square)
        {
            char dot = '*';

            int newX = square.X;
            for (int sideSise = square.Size; sideSise > 0; sideSise--)
            {
                canvas[square.Y][newX] = dot;
                canvas[square.Y + (square.Size - 1)][newX] = dot;
                newX++;
            }

            int newY = square.Y;
            for (int sideSise = square.Size; sideSise > 0; sideSise--)
            {
                canvas[newY][square.X] = dot;
                canvas[newY][square.X + (square.Size - 1)] = dot;
                newY++;
            }

            for (int i = 0; i < canvas.Length; i++)
            {
                for (int j = 0; j < canvas[0].Length; j++)
                {
                    Console.Write(canvas[i][j]);
                }
                Console.WriteLine();
            }
        }
    }
}
