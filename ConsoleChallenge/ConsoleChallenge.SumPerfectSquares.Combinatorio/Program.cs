﻿namespace ConsoleChallenge.SumPerfectSquares.Combinatorio
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            int[] getIn = { 1, 2, 3, 4, 5 };
            var result = BuscarCombinaciones(getIn, 3);
        }
        public static List<List<int>> BuscarCombinaciones(int[] arr, int x)
        {
            bool[] utilizados = new bool[arr.Length];
            List<List<int>> res = new List<List<int>>();
            BuscarCombinaciones(arr, x, utilizados, 0, res);
            ImprimirCombinaciones(res);
            return res;
        }

        private static void ImprimirCombinaciones(List<List<int>> res)
        {
            foreach (var item in res)
            {
                foreach (var x in item)
                {
                    Console.Write(x+" ");
                }
                Console.WriteLine();
            }
        }

        private static void BuscarCombinaciones(int[] arr, int x, bool[] utilizados, int posAct, List<List<int>> res)
        {
            if (x == 0)
            {
                List<int> combinacion = new List<int>();
                for (int i = 0; i < arr.Length; i++)
                {
                    if (utilizados[i] == true)
                    {
                        combinacion.Add(arr[i]);
                    }
                }
                res.Add(combinacion);
            }
            else
            {
                for (int pos = posAct; pos < arr.Length; pos++)
                {
                    utilizados[pos] = true;
                    BuscarCombinaciones(arr, x - 1, utilizados, pos + 1, res);
                    utilizados[pos] = false;
                }
            }
        }

    }
}